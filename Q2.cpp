#include <stdio.h>
#include <vector>
#include <algorithm>
#include <string>
#include <queue>

using namespace std;

class HuffmanCodes
{
	struct NewNode
	{
		char data;
		size_t freq;
		NewNode* left;
		NewNode* right;
		NewNode(char data, size_t freq):data(data),freq(freq),left(NULL), right(NULL) {}

		~NewNode()
		{
			delete left;
			delete right;
		}
	};

	struct Compare
	{
		bool operator()(NewNode* lsh, NewNode* rhs)
		{
			return (lsh->freq > rhs->freq);
		}

	};

	NewNode* top;

	void printCode(NewNode* root, string str)
	{
		if (root == NULL)
			return;

		if (root->data == '$')
		{
			printCode(root->left, str + "0");
			printCode(root->right, str + "1");
		}
	}

public:
	HuffmanCodes(){}

	~HuffmanCodes()
	{
		delete top;
	}

};

///------------------------------------------------------
/// Given some data where each data element is represented by
/// an 8-bit fixed-length codeword, run the Huffman coder
/// and return the length of the variable-length codeword for each
/// of the 256 symbols. Also return the total number of bits required
/// to encode the data using the variable-length codewords
/// Requirement: You must make use of a min-heap in your implementation
///
int HuffmanCoder(vector<unsigned char>& data, vector<unsigned char>& codewordLengths) {
	// Fill this in
	//https://www.studytonight.com/data-structures/huffman-coding
	return 0;
} //end-HuffmanCoder