#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

extern int EditDistance(string& X, string& Y);

///-----------------------------------------------------
/// Test1
///
int Q5Test1() {
	string X = "backache";
	string Y = "sackrace";

	if (EditDistance(X, Y) != 3) return 0;

	return 10;
} //end-Q5Test1

///------------------------------------------------------
/// Test for EditDistance
///
int Q5Test() {
	int grade = 0;

	grade += Q5Test1();

	return grade;
} //end-Q5Test