#include <stdio.h>
#include <vector>
#include <algorithm>

using namespace std;

extern int MinCoinExchange(vector<int> &coins, int amount);

///-----------------------------------------------------
/// Test1
///
int Q1Test1() {
	vector<int> coins = { 5, 1, 25, 10 };
	int amount = 68;

	int noCoins = MinCoinExchange(coins, amount);
	if (noCoins == 7) return 50;

	return 0;
} //end-Q1Test1

///-----------------------------------------------------
/// Test2
///
int Q1Test2() {
	vector<int> coins = { 5, 1, 50, 25, 10 };
	int amount = 77;

	int noCoins = MinCoinExchange(coins, amount);
	if (noCoins == 4) return 50;

	return 0;
} //end-Q1Test2

///------------------------------------------------------
/// Test for MinCoinExchange
///
int Q1Test() {
	int grade = 0;

	grade += Q1Test1();
	grade += Q1Test2();

	return grade;
} //end-Q1Test