#include <stdio.h>
#include <vector>
#include <algorithm>

using namespace std;

enum DIR { DOWN = 1, RIGHT = 2 };
struct Index { int i, j; Index(int _i, int _j) { i = _i; j = _j; } };

///------------------------------------------------------
/// You are given a NxN matrix with each slot containing a number 
/// indicating the cost of going through that slot
/// Initially you are at(0, 0)
/// You are only allowed to go Down or Right
/// You want to go to(N - 1, N - 1)
/// Find the path that has the minimum total cost
///
int MinimumCostPath(vector<vector<int>>& M, vector<Index>& Path) {
	// Fill this in

	return 0;
} //end-MinimumCostPath