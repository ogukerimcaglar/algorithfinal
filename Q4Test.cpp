#include <stdio.h>
#include <vector>
#include <algorithm>

using namespace std;

extern int MinCoinExchangeDP(vector<int> &coins, int amount);

///-----------------------------------------------------
/// Test1
///
int Q4Test1() {
	// Greedy would return: 2x25 + 6x3 = 8 coins
	// Optimal Solution:    1x25 + 2x20 + 1x3 = 4 coins
	vector<int> coins = { 1, 3, 20, 25 };  
	int amount = 68;

	int noCoins = MinCoinExchangeDP(coins, amount);
	if (noCoins == 4) return 10;

	return 0;
} //end-Q4Test1

///-----------------------------------------------------
/// Test2
///
int Q4Test2() {
	// Greedy would return: 2x30 + 1x10 + 1x5 + 2x1 = 6 coins
	// Optimal solution:    4x18 + 1x5 = 5 coins
	vector<int> coins = { 1, 5, 10, 18, 30 }; 
	int amount = 77;

	int noCoins = MinCoinExchangeDP(coins, amount);
	if (noCoins == 5) return 15;

	return 0;
} //end-Q4Test2

///------------------------------------------------------
/// Test for MinCoinExchangeDP Test
///
int Q4Test() {
	int grade = 0;

	grade += Q4Test1();
	grade += Q4Test2();

	return grade;
} //end-Q4Test