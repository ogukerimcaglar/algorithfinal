#include <stdio.h>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

int findSmalest(int arr[])
{
	int temp = arr[0];
	int n = sizeof(arr) / sizeof(arr[0]);
	for (int i = 0; i < n; i++)
	{
		if (temp > arr[i]) {
			temp = arr[i];
		}
	}
	return temp;
}

int findNumberCoins(vector<int>& coins, int amount, vector<int>& results)
{
	int min = INT_MAX;
	//Check amount in results
	if (count(results.begin(), results.end(), amount))
	{
		return results[amount];
	}
	//Check if amount in coins
	if (count(coins.begin(), coins.end(), amount))
	{
		return results[amount] = 1;
	}

	else
	{
		for (int i = 0; i < coins.size(); i++)
		{
			if (coins[i] <= amount)
			{
				int subResult = findNumberCoins(coins, amount - coins[i], results);
				if (subResult + 1 < min)
				{
					min = subResult + 1;
				}
			}
		}

		results[amount] = min;
		return min;
	}
}

///------------------------------------------------------
/// Design a dynamic programming algorithm that given a set of coins that includes 1
/// returns the minimum number of coins necessary to represent the amount
///
int MinCoinExchangeDP(vector<int> &coins, int amount) {
	// Fill this in
	vector <int> result;

	for (int i = 1; i < amount; i++)
	{
		findNumberCoins(coins, i, result);
	}

	return findNumberCoins(coins, amount, result);

} //end-MinCoinExchangeDP
