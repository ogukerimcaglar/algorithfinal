#include <stdio.h>
#include <vector>
#include <algorithm>
using namespace std;

///------------------------------------------------------
/// Design a greedy algorithm that given a set of coins that includes 1
/// returns the minimum number of coins necessary to represent the amount
/// Pre-requisites: coins are NOT given in sorted order
/// Requirement: If you have "n" coins, your algorithm must run in O(nlogn)
///
int MinCoinExchange(vector<int> &coins, int amount) {
	// Fill this in
	int n = coins.size();
	sort(coins.begin(),coins.end());
	vector<int> result;

	for (int i = n-1; i >= 0; i--)
	{
		while (amount >= coins[i])
		{
			amount -= coins[i];
			result.push_back(coins[i]);
		}
	}

	return result.size();
} //end-MinCoinExchange
