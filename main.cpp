#include <stdio.h>
#include <vector>

using namespace std;

extern int Q1Test();
extern int Q4Test();

int main() {
	double grade = 0;
	double Q[] = { 0.10, 0.10 };

	grade += Q[0] * Q1Test() + Q[1]*Q4Test();

	printf("Your final exam grade is %.1lf\n", grade);

	return 0;
} //end-main